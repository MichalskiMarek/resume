import { Button, Link } from "@nextui-org/react";
import { useTranslation } from "@/app/i18n/server";
import { LngParams } from "@/components/types";

type NotFoundProps = LngParams;

export default async function NotFound({ params: { lng } }: NotFoundProps) {
  const { t } = await useTranslation(lng);
  return (
    <section className="bg-white dark:bg-black">
      <div className="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
        <div className="mx-auto max-w-screen-sm text-center">
          <h1 className="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-primary-600 dark:text-primary-500">
            404
          </h1>
          <p className="mb-4 text-3xl tracking-tight font-bold text-gray-900 md:text-4xl dark:text-white">
            {t("notFound.somethingMissing")}
          </p>
          <p className="mb-4 text-lg font-light text-gray-500 dark:text-gray-400">
            {t("notFound.sorry")}
          </p>

          <Button className={"bg-primary-600 text-white"} as={Link} href={`/`}>
            {t("notFound.back")}
          </Button>
        </div>
      </div>
    </section>
  );
}
