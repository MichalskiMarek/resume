import { useTranslation } from "@/app/i18n/server";
import { LngParams } from "@/components/types";

export default async function About(props: LngParams) {
  const {
    params: { lng },
  } = props;
  const { t } = await useTranslation(lng);

  const techs = ["TypeScript", "Next.js", "NextUI", "TailwindCSS", "i18n"];
  const links = [
    {
      name: t("about.thisRepo"),
      url: "https://gitlab.com/MichalskiMarek/resume",
    },
    {
      name: "LinkedIn",
      url: "https://www.linkedin.com/in/marek-michalski-0a1364149/",
    },
  ];

  return (
    <article className="max-w-[600px] mx-auto pt-12 px-2">
      <h2 className="text-3xl font-bold mb-6">{t("about.title")}</h2>
      <p className="mb-6">
        <strong>{t("about.text1")} </strong>
        {t("about.text2")}
      </p>
      <p className="font-bold mb-6">{t("about.listTitle")}</p>
      <ul className="list-disc ml-4 mb-6">
        {techs.map((tech) => (
          <li className="mb-1" key={tech}>
            {tech}
          </li>
        ))}
      </ul>
      <p>
        <strong>{t("about.text3")} </strong>
        {t("about.text4")}
      </p>
      <p className="font-bold my-6">{t("about.text5")}</p>
      <p className="font-bold mb-6">{t("about.myLinks")}</p>
      <ul className="list-disc ml-4 mb-6">
        {links.map((link) => (
          <li className="mb-1" key={link.name}>
            <a
              href={link.url}
              target="_blank"
              rel="noreferrer"
              className="text-blue-500"
            >
              {link.name}
            </a>
          </li>
        ))}
      </ul>
    </article>
  );
}
