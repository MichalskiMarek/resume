import { Resume } from "@/components/resume/Resume";
import { LngParams } from "@/components/types";

type HomeProps = LngParams;

export default async function Home(props: HomeProps) {
  return <Resume {...props} />;
}
