import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "../globals.css";
import { ReactNode } from "react";
import { dir } from "i18next";
import NavbarComponent from "@/components/navbar/Navbar";
import { Providers } from "@/app/providers";
import { LngParams } from "@/components/types";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "CV Marek Michalski",
  description: "Marek Michalski - Software Engineer",
};

type RootLayoutProps = Readonly<
  {
    children: ReactNode;
  } & LngParams
>;

export default function RootLayout(props: RootLayoutProps) {
  const { children, params } = props;
  const { lng } = params;

  return (
    <html suppressHydrationWarning lang={lng} dir={dir(lng)}>
      <head>
        <title>Marek Michalski</title>
      </head>
      <body className={`${inter.className} bg-transparent`}>
        <Providers>
          <NavbarComponent lng={lng} />
          {children}
        </Providers>
      </body>
    </html>
  );
}
