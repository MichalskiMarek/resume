export enum Languages {
  pl = "pl",
  en = "en",
}

export type LngParams = {
  params: {
    lng: Languages;
  };
};

export type SectionRow = { [key: string]: string } | string;

export type SectionItem = {
  [section: string]: { [lng in Languages]?: SectionRow[] };
};

export type SectionProps = {
  item: SectionItem;
} & LngParams;
