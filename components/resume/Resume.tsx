import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Divider,
} from "@nextui-org/react";
import NextImage from "next/image";
import profilePhoto from "../../public/profile_photo.jpg";
import { LngParams, SectionItem } from "../types";
import { ResumeSection } from "./ResumeSection";
import { useTranslation } from "@/app/i18n/server";
import data from "../../app/data.json";

export const Resume = async (props: LngParams) => {
  const { params } = props;
  const { lng } = params;
  const { t } = await useTranslation(lng);
  const resumeData = data as unknown as SectionItem[];

  return (
    <main className="flex justify-center items-start pt-3 sm:pt-12 px-2 bg-white dark:bg-black">
      <Card className="w-[860px]">
        <CardHeader className="flex flex-col xs:flex-row justify-around h-56 xs:h-36">
          <NextImage
            alt="Marek Michalski - profile photo"
            height={120}
            src={profilePhoto}
            width={120}
            className="rounded-full"
          />
          <div className="flex flex-col items-center xs:items-start">
            <p className="text-4xl">Marek Michalski</p>
            <p className="text-xl text-default-500">
              {t("home.softwareEngineer")}
            </p>
          </div>
        </CardHeader>
        <Divider />
        <CardBody className="flex sm:grid sm:grid-cols-[1fr,_2fr] grid-rows-12 sm:grid-rows-9 gap-y-4 sm:gap-2">
          {resumeData.map((item) => {
            const key = Object.keys(item)[0];
            return <ResumeSection item={item} {...props} key={key} />;
          })}
        </CardBody>
        <Divider />
        <CardFooter></CardFooter>
      </Card>
    </main>
  );
};
