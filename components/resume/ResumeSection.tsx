import { useTranslation } from "@/app/i18n/server";
import { Divider } from "@nextui-org/react";
import { Languages, SectionProps } from "../types";
import { sanitize } from "isomorphic-dompurify";

export const ResumeSection = async (props: SectionProps) => {
  const { params, item } = props;

  const { lng } = params;
  const { t } = await useTranslation(lng);

  const title = Object.keys(item)[0];
  const section = item[title][lng] ?? item[title][Languages.en];

  return (
    <section className={title}>
      <h3 className="text-xl pb-3">
        {t(`home.${title}`)}
        <Divider />
      </h3>
      <ul>
        {section?.map((section) => {
          if (typeof section === "string") {
            return (
              <li
                className="font-light text-sm list-disc ml-4 pb-1"
                key={section}
                dangerouslySetInnerHTML={{
                  __html: sanitize(section),
                }}
              />
            );
          } else {
            const key = Object.keys(section)[0];
            return (
              <li className="font-semibold text-sm pb-2" key={key}>
                {t(`home.${key}`)}:{" "}
                <span
                  className="font-light"
                  dangerouslySetInnerHTML={{
                    __html: sanitize(section[key]),
                  }}
                />
              </li>
            );
          }
        })}
      </ul>
    </section>
  );
};
