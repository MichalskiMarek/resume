import { NavbarBrand } from "@nextui-org/react";
import { Logo } from "@/components/logo/Logo";

export const NavBrand = () => {
  return (
    <NavbarBrand>
      <div className="font-semibold flex items-center gap-2 uppercase">
        <Logo />
        <div className="flex-col hidden xs:flex">
          <h1 className="text-2xl text-[#5f1854] dark:text-white">Marek</h1>
          <h2 className="text-xs text-[#a12559] dark:text-white">Michalski</h2>
        </div>
      </div>
    </NavbarBrand>
  );
};
