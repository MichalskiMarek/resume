'use client';

import { Link, NavbarItem } from '@nextui-org/react';

type Props = {
	href: string;
	text: string;
	isActive?: boolean;
};
export const NavItem = (props: Props) => {
	const { href, text, isActive } = props;

	return (
		<NavbarItem isActive={isActive}>
			<Link color="foreground" href={href} className={'capitalize'}>
				{text}
			</Link>
		</NavbarItem>
	);
};
