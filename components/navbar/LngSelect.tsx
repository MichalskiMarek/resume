import { languages } from "@/app/i18n/settings";
import { Select, SelectItem } from "@nextui-org/react";
import { usePathname, useRouter } from "next/navigation";
import { Languages } from "../types";

type LngSelectProps = {
  lng: Languages;
};
export const LngSelect = (props: LngSelectProps) => {
  const { lng } = props;
  const router = useRouter();
  const pathname = usePathname();

  return (
    <Select
      size={"sm"}
      variant="underlined"
      className="w-14 uppercase"
      aria-label="Language select"
      defaultSelectedKeys={[lng]}
      classNames={{ value: "uppercase", listboxWrapper: "min-w-fit" }}
      onChange={(event) =>
        router.push(pathname.replace(lng, event.target.value))
      }
    >
      {languages.map((lng) => (
        <SelectItem key={lng} value={lng} className="uppercase">
          {lng}
        </SelectItem>
      ))}
    </Select>
  );
};
