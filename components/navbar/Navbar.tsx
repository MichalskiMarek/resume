"use client";

import { NavBrand } from "@/components/navbar/NavBrand";
import { NavItems } from "@/components/navbar/NavItems";
import {
  Navbar,
  NavbarContent,
  NavbarMenu,
  NavbarMenuToggle,
} from "@nextui-org/react";
import { useState } from "react";
import { LngSelect } from "./LngSelect";
import { Languages } from "../types";
import { ThemeSwitcher } from "./ThemeSwitcher";
import { useTheme } from "next-themes";

type NavbarComponentProps = {
  lng: Languages;
};

export default function NavbarComponent(props: NavbarComponentProps) {
  const { lng } = props;
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { theme } = useTheme();
  return (
    <Navbar
      isBordered
      isMenuOpen={isMenuOpen}
      onMenuOpenChange={setIsMenuOpen}
      height={"5rem"}
      maxWidth="full"
      className="bg-transparent"
    >
      <NavbarContent className="sm:hidden" justify="start">
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
        />
      </NavbarContent>

      <NavbarContent className="sm:hidden pr-3" justify="center">
        <NavBrand />
      </NavbarContent>

      <NavbarContent className="hidden sm:flex gap-10 h-48" justify="center">
        <NavBrand />
        <div className="flex gap-4">
          <NavItems lng={lng} />
        </div>
        <LngSelect lng={lng} />
        <ThemeSwitcher />
      </NavbarContent>

      <NavbarMenu>
        <NavItems lng={lng} />
        <ThemeSwitcher />
        <LngSelect lng={lng} />
      </NavbarMenu>
    </Navbar>
  );
}
