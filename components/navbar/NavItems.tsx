import { routes } from "@/utils/routes";
import { NavItem } from "@/components/navbar/NavItem";
import { useTranslation } from "@/app/i18n/client";
import { usePathname } from "next/navigation";
import { Languages } from "../types";

type NavItemsProps = {
  lng: Languages;
};

export const NavItems = (props: NavItemsProps) => {
  const { lng } = props;
  const { t } = useTranslation(lng);
  const pathname = usePathname();

  const routesKeys = Object.keys(routes) as (keyof typeof routes)[];

  return (
    <>
      {routesKeys.map((route) => {
        const href = `/${lng}${routes[route] ? `/${routes[route]}` : ""}`;

        return (
          <NavItem
            text={t(`${route}.nav`)}
            href={href}
            key={route}
            isActive={href === pathname}
          />
        );
      })}
    </>
  );
};
