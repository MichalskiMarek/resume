import type { Config } from "tailwindcss";
import { nextui } from "@nextui-org/react";
import defaultTheme from "tailwindcss/defaultTheme";

const colors = {
  focus: "#FF71DD",
  primary: {
    50: "#FFE6F5",
    100: "#FFC1E5",
    200: "#FF9CE1",
    300: "#FF71DD",
    400: "#FF42C9",
    500: "#CA2F70",
    600: "#BB2767",
    700: "#A51D5B",
    800: "#98194A",
    900: "#8B1538",
    DEFAULT: "#A51D5B",
  },
};

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  plugins: [nextui()],
  darkMode: "class",
  theme: {
    screens: {
      xs: "475px",
      ...defaultTheme.screens,
    },
    extend: {
      colors,
    },
  },
};
export default config;
